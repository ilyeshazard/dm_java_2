import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class BibDM{

    /**
     * Ajoute deux entiers
     * @param a le premier entier à ajouter
     * @param b le deuxieme entier à ajouter
     * @return la somme des deux entiers
     */
    public static Integer plus(Integer a, Integer b){
        return a + b;
    }


    /**
     * Renvoie la valeur du plus petit élément d'une liste d'entiers
     * VOUS DEVEZ LA CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
     * @param liste
     * @return le plus petit élément de liste
     */
    public static Integer min(List<Integer> liste){
      Integer minVal = null;
      for (int i:liste)
        if (minVal == null || i < minVal)
          minVal = i;
        return minVal;
    }


    /**
     * Teste si tous les élements d'une liste sont plus petits qu'une valeure donnée
     * @param valeur
     * @param liste
     * @return true si tous les elements de liste sont plus grands que valeur.
     */
    public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T valeur , List<T> liste){
        for (T objet : liste)
          if (objet.compareTo(valeur) <= 0)
            return false;
        return true;
    }



    /**
     * Intersection de deux listes données par ordre croissant.
     * @param liste1 une liste triée
     * @param liste2 une liste triée
     * @return une liste triée avec les éléments communs à liste1 et liste2
     */
    public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2){
        List<T> listInteger = new ArrayList<>();
        int cpt1 = 0;
        int cpt2 = 0;
        while(cpt1 < liste1.size() && cpt2 < liste2.size()) {
          if (liste1.get(cpt1).equals(liste2.get(cpt2))) {
            if(!listInteger.contains(liste1.get(cpt1)))
              listInteger.add(liste1.get(cpt2));
            cpt1++;
            cpt2++;
          }
          else if(liste1.get(cpt1).equals(liste2.get(cpt2))) {
            if(!listInteger.contains(liste1.get(cpt1)))
              listInteger.add(liste1.get(cpt1));
            cpt1++;
            cpt2++;
          }
          else{
            if(liste1.get(cpt1).compareTo(liste2.get(cpt2)) < 0)
              cpt1++;
            else
              cpt2++;
          }
        }
        return listInteger;
    }


    /**
     * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
     * @param texte une chaine de caractères
     * @return une liste de mots, correspondant aux mots de texte.
     */
    public static List<String> decoupe(String texte){
      String[] caracteres = null;
      caracteres = texte.split(" ");
      List<String> liste = new ArrayList<>();
      for (String c : caracteres)
          if (!c.equals(""))
              liste.add(c);
      return liste;
    }


    /**
     * Renvoie le mot le plus présent dans un texte.
     * @param texte une chaine de caractères
     * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
     */

    public static String motMajoritaire(String texte){
      Map<String, Integer> dico = new HashMap<>();
      String motMax;
      for(String mot : decoupe(texte)){
        if(dico.containsKey(mot))
          dico.put(mot, dico.get(mot)+1);
        else
          dico.put(mot, 1);
      }
      if(dico.size() > 0){
        Map.Entry<String, Integer> maxEntry = null;
        for (Map.Entry<String, Integer> entry : dico.entrySet()) {
          if (maxEntry == null || entry.getValue().compareTo(maxEntry.getValue()) == 0 && entry.getKey().compareTo(maxEntry.getKey()) < 0)
            maxEntry = entry;
          if (maxEntry == null || entry.getValue().compareTo(maxEntry.getValue()) > 0)
            maxEntry = entry;
        }
        motMax = maxEntry.getKey();
        return motMax;
      }
      else
        return null;
    }

    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de ( et de )
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
     */
    public static boolean bienParenthesee(String chaine){
      int cpt = 0;
      for (int i = 0; i < chaine.length(); i++) {
          if (chaine.charAt(i) == '(') /*charAt c le carctère de l'indice i */
              cpt++;
          else if (chaine.charAt(i) == ')')
              cpt--;
          if (cpt < 0)
              return false;
      }
      if (cpt == 0)
          return true;
      return false;
    }


    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors ue ([]) est bien parenthèsée.
     */
    public static boolean bienParentheseeCrochets(String chaine){
      int cptParenthese = 0;
      int cptCrochet = 0;
      List<String> enAttente = new ArrayList<>();
      for (int i = 0; i < chaine.length(); i++) {
          if (chaine.charAt(i) == '(') {
              cptParenthese++;
              enAttente.add("(");
          }
          if (chaine.charAt(i) == ')') {
              cptParenthese--;
              if (enAttente.isEmpty() || enAttente.get(enAttente.size() - 1).equals("["))
                  return false;
              enAttente.remove(enAttente.size() - 1);
          }
          if (chaine.charAt(i) == '[') {
              cptCrochet++;
              enAttente.add("[");
          }
          if (chaine.charAt(i) == ']') {
              cptCrochet--;
              if (enAttente.isEmpty() || enAttente.get(enAttente.size() - 1).equals("("))
                  return false;
              enAttente.remove(enAttente.size() - 1);
          }
          if (cptParenthese < 0 || cptCrochet < 0)
              return false;
      }
      if (cptParenthese == 0 && cptCrochet == 0)
          return true;
      return false;

    }


    /**
     * Recherche par dichtomie d'un élément dans une liste triée
     * @param liste, une liste triée d'entiers
     * @param valeur un entier
     * @return true si l'entier appartient à la liste.
     */
    public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur){
      if (liste.isEmpty()) /* vérifie si la liste est vide*/
          return false;
      int Inf = 0;
      int Sup = liste.size() - 1;
      int milieu = (Inf + Sup) / 2;
      while (Inf <= Sup) {
          if (liste.get(milieu) == valeur)
              return true;
          if (liste.get(milieu) < valeur)
              Inf = milieu + 1;
          else
              Sup = milieu - 1;
          milieu = (Inf + Sup) / 2;
      }
      return false;
    }

}
